import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/constants.dart';
import 'package:flickr_search/ui/home_screen/home_screen.dart';
import 'package:flickr_search/utils/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

void main() {
  setupServiceLocator();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SearchBloc>(create: (context) => getIt<SearchBloc>()),
      ],
      child: MaterialApp(
        title: 'Flickr Search',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.purpleAccent.shade100),
          useMaterial3: true,
        ),
        home: const HomeScreen(),
      ),
    );
  }
}
