import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/api_urls.dart';
import 'package:flickr_search/entities/constants.dart';
import 'package:flickr_search/repositories/apis/search_api_client.dart';
import 'package:flickr_search/repositories/search_repository/seach_repository.dart';
import 'package:http/http.dart' as http;

void setupServiceLocator() {
  getIt.registerSingleton(SearchApiClient());
  getIt.registerSingleton(SearchRepository(searchApiClient: getIt<SearchApiClient>()));
  getIt.registerSingleton(SearchBloc(searchRepository: getIt<SearchRepository>()));
  getIt.registerSingleton(http.Client());
  getIt.registerSingleton(ApiUrl());
}
