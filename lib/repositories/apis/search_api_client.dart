import 'package:flickr_search/entities/constants.dart';
import 'package:http/http.dart' as http;

class SearchApiClient {
  Future<http.Response> getPhotosData(Uri url) async {
    late http.Response response;
    try {
      response = await getIt<http.Client>().get(url);
    } catch (e) {
      response = http.Response("SERVER ERROR", 500);
    }
    return response;
  }

  Future<http.Response> getLicenceData(Uri url) async {
    late http.Response response;
    try {
      response = await getIt<http.Client>().get(url);
    } catch (e) {
      response = http.Response("SERVER ERROR", 500);
    }
    return response;
  }
}
