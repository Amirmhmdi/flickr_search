import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flickr_search/entities/data_transfer_object/failure.dart';
import 'package:flickr_search/entities/data_transfer_object/licence/licence.dart';
import 'package:flickr_search/entities/data_transfer_object/photos_search.dart';
import 'package:flickr_search/repositories/apis/search_api_client.dart';
import 'package:http/http.dart' as http;

class SearchRepository {
  final SearchApiClient searchApiClient;
  SearchRepository({required this.searchApiClient});

  Future<Either<Failure, PhotosSearch>> getPhotosData(Uri url) async {
    http.Response response = await searchApiClient.getPhotosData(url);

    switch (response.statusCode) {
      case 200:
        PhotosSearch photosSearch = PhotosSearch.fromJson(jsonDecode(response.body));
        return Right(photosSearch);
      case 500:
        return Left(ServerFailure());
      default:
        return Left(ResultFailure(statusCode: response.statusCode));
    }
  }

  Future<Either<Failure, LicenseClass>> getLicenceData(Uri url) async {
    http.Response response = await searchApiClient.getLicenceData(url);

    switch (response.statusCode) {
      case 200:
        LicenseClass licenseObj = LicenseClass.fromJson(jsonDecode(response.body));
        return Right(licenseObj);
      case 500:
        return Left(ServerFailure());
      default:
        return Left(ResultFailure(statusCode: response.statusCode));
    }
  }
}
