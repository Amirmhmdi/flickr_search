import 'package:flutter/material.dart';

Future<int> datePickerBox(BuildContext context) async {
  DateTime currentDate = DateTime.now();
  final DateTime? pickedDate = await showDatePicker(context: context, initialDate: currentDate, firstDate: DateTime(2015), lastDate: DateTime(2050));
  if (pickedDate != null) {
    return pickedDate.millisecondsSinceEpoch;
  } else {
    return -1;
  }
}
