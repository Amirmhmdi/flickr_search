import 'package:flutter/material.dart';

class ComboBoxWidget extends StatefulWidget {
  final String lable;
  final List<String> itemsList;
  String selectedValue;
  final Function(String) onSelected;
  ComboBoxWidget({super.key, required this.itemsList, required this.onSelected, required this.lable, required this.selectedValue});

  @override
  State<ComboBoxWidget> createState() => _ComboBoxWidgetState();
}

class _ComboBoxWidgetState extends State<ComboBoxWidget> {
  final TextEditingController _textEditingController = TextEditingController();

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: DropdownMenu<String>(
        inputDecorationTheme: InputDecorationTheme(
          isDense: true,
          contentPadding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          constraints: BoxConstraints.tight(const Size.fromHeight(40)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        initialSelection: widget.selectedValue,
        controller: _textEditingController,
        requestFocusOnTap: false,
        label: Text(
          widget.lable,
          style: const TextStyle(color: Colors.grey, fontSize: 12),
        ),
        onSelected: (String? item) {
          setState(() {
            widget.selectedValue = item ?? "";
          });
          widget.onSelected(item ?? "");
        },
        dropdownMenuEntries: widget.itemsList.map<DropdownMenuEntry<String>>((String item) {
          return DropdownMenuEntry<String>(
            value: item,
            label: item,
            enabled: true,
            style: MenuItemButton.styleFrom(
              foregroundColor: Colors.purpleAccent.shade100.withOpacity(0.5),
            ),
          );
        }).toList(),
      ),
    );
  }
}
