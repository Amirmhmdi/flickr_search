import 'package:flutter/material.dart';

class CheckboxThreeValue extends StatefulWidget {
  final bool? value;
  final ValueChanged<bool?> onChanged;

  const CheckboxThreeValue({
    super.key,
    required this.value,
    required this.onChanged,
  });

  @override
  State<CheckboxThreeValue> createState() => _CheckboxThreeValueState();
}

class _CheckboxThreeValueState extends State<CheckboxThreeValue> {
  bool? _checked;

  @override
  void initState() {
    super.initState();
    _checked = widget.value;
  }

  @override
  void didUpdateWidget(CheckboxThreeValue oldWidget) {
    super.didUpdateWidget(oldWidget);
    _checked = widget.value;
  }

  Widget _buildIcon() {
    Color? fillColor;
    Color? iconColor;
    IconData? iconData;

    var checked = _checked;
    if (checked != null) {
      if (checked) {
        fillColor = Colors.purple;
        iconColor = Colors.white;
        iconData = Icons.check;
      } else {
        fillColor = Colors.purple;
        iconColor = Colors.white;
        iconData = Icons.close;
      }
    } else {
      fillColor = Colors.white;
      iconColor = Colors.grey;
      iconData = Icons.remove;
    }

    return Container(
      decoration: BoxDecoration(
        color: fillColor,
        border: Border.all(color: Colors.black87, width: 2),
        borderRadius: const BorderRadius.all(Radius.circular(2)),
      ),
      child: Icon(
        iconData,
        size: 15,
        color: iconColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: _buildIcon(),
      onPressed: () {
        bool? result = _checked;
        widget.onChanged(
          result == null
              ? true
              : result
                  ? false
                  : null,
        );
      },
      highlightColor: Colors.transparent,
    );
  }
}
