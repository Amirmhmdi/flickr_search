import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/ui/home_screen/widgets/filters_list_widget.dart';
import 'package:flickr_search/ui/home_screen/widgets/search_text_field.dart';
import 'package:flickr_search/ui/search_terms_screen/search_terms_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late SearchBloc _searchBloc;
  late TextEditingController _textEditingController;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    _textEditingController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            title: SearchTextField(textEditingController: _textEditingController),
            elevation: 1,
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(30.0),
              child: Container(
                alignment: Alignment.center,
                height: 30.0,
                padding: const EdgeInsets.only(left: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    OutlinedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (ctx) => const SearchTermsScreen(),
                          ),
                        );
                      },
                      style: OutlinedButton.styleFrom(
                        padding: const EdgeInsets.all(4.0),
                        side: const BorderSide(width: 1.0, color: Colors.purple),
                      ),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "filter",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Icon(Icons.filter_alt_rounded),
                        ],
                      ),
                    ),
                    const VerticalDivider(color: Colors.purple),
                    const Expanded(child: FilterListWidget()),
                  ],
                ),
              ),
            )
            // Container(child: Text("data")),
            ),
        body: BlocConsumer<SearchBloc, SearchState>(
          listener: (context, state) {
            if (state is SearchErrorState) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message),
                  backgroundColor: Colors.red,
                ),
              );
            }
          },
          builder: (context, state) {
            if (state is SearchLoadingState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is SearchLoadedState) {
              return Container(
                child: (_searchBloc.photosSearch?.photos?.photo?.length ?? 0) != 0
                    ? GridView.builder(
                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                        itemCount: _searchBloc.photosSearch?.photos?.photo?.length ?? 0,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: const EdgeInsets.all(3),
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Stack(
                                children: [
                                  Positioned.fill(
                                    child: InkWell(
                                      child: Image.network(
                                        _searchBloc.photosSearch?.photos?.photo?[index].getThumbnail() ?? "",
                                        fit: BoxFit.cover,
                                      ),
                                      onTap: () {
                                        //TODO: show image in Single page with all data
                                      },
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            Colors.transparent,
                                            Colors.purple.withOpacity(0.2),
                                            Colors.purple.withOpacity(0.4),
                                            Colors.purple.withOpacity(0.8),
                                          ],
                                        ),
                                      ),
                                      child: Text(
                                        _searchBloc.photosSearch?.photos?.photo?[index].title ?? "",
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(color: Colors.white, fontSize: 10),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      )
                    : Container(
                        alignment: Alignment.center,
                        child: const Text("Nothing to Show"),
                      ),
              );
            } else if (state is SearchErrorState) {
              return Container(
                alignment: Alignment.center,
                child: TextButton(
                  child: Text(
                    state.message,
                    style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  onPressed: () {},
                ),
              );
            } else {
              return Container(
                alignment: Alignment.center,
                child: const Text("Type Some thing above to seach photo"),
              );
            }
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    _searchBloc.close();
    super.dispose();
  }
}
