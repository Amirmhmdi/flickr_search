import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchTextField extends StatelessWidget {
  final TextEditingController textEditingController;
  const SearchTextField({super.key, required this.textEditingController});

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder border = const OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      borderSide: BorderSide(color: Colors.purple),
    );

    return TextField(
      controller: textEditingController,
      scrollPadding: const EdgeInsets.all(0),
      cursorColor: Colors.black,
      textInputAction: TextInputAction.search,
      style: const TextStyle(color: Colors.black, fontSize: 15.0),
      decoration: InputDecoration(
        hintText: "Search Photo ...",
        contentPadding: const EdgeInsets.only(top: 0.0, bottom: 0.0, left: 16.0, right: 0.0),
        border: border,
        disabledBorder: border,
        enabledBorder: border,
        errorBorder: border,
        focusedBorder: border,
        suffixIcon: const Icon(Icons.search, color: Colors.grey),
      ),
      onSubmitted: (text) {
        BlocProvider.of<SearchBloc>(context).add(NewSearchEvent(searchText: text));
      },
    );
  }
}
