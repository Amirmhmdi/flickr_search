import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/contact_widget/contact_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/content_type_widget/content_type_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/geo_context_widget/geo_context_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/get_time_widget/get_time_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/group_id_widget/group_id_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/has_geo_widget/has_geo_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/in_gallery_widget.dart/in_gallery_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/is_commons_widget/is_commons_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/license_widget/license_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/machine_tag_mode/machine_tag_mode.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/media_widget/media_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/place_id_widget/place_id_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/privacy_filter_widget/privacy_filter_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/safe_search_widget/safe_search_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/sort_widget/sort_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/tags_widgets/tags_widget.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/video_content_type/video_content_type.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/woe_id_widget/woe_id_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchTermsScreen extends StatefulWidget {
  const SearchTermsScreen({super.key});

  @override
  State<SearchTermsScreen> createState() => _SearchTermsScreenState();
}

class _SearchTermsScreenState extends State<SearchTermsScreen> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context)..add(FetchLicanceEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            "Search Terms",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          actions: [
            OutlinedButton(
              onPressed: () {
                if (_searchBloc.filter.text != "") {
                  BlocProvider.of<SearchBloc>(context).add(NewSearchEvent());
                  Navigator.of(context).pop();
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Please Enter your search Text in previus screen"),
                      backgroundColor: Colors.red,
                    ),
                  );
                }
              },
              style: OutlinedButton.styleFrom(
                padding: const EdgeInsets.all(4.0),
                side: const BorderSide(width: 1.0, color: Colors.purple),
              ),
              child: const Text("Search"),
            ),
          ],
          elevation: 1,
        ),
        body: const Padding(
          padding: EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                TagsWidget(),
                Divider(),
                GetTimeWidget(),
                Divider(),
                LicenseWidget(),
                Divider(),
                SortWidget(),
                Divider(),
                PrivacyFilterWidget(),
                Divider(),
                //TODO: BBox Widget
                SafeSearchWidget(),
                Divider(),
                ContentTypeWidget(),
                Divider(),
                VideoContentTypeWidget(),
                Divider(),
                MachineTagModeWidget(),
                Divider(),
                GroupIdWidget(),
                Divider(),
                ContactWidget(),
                Divider(),
                WoeIdWidget(),
                Divider(),
                PlaceIdWidget(),
                Divider(),
                MediaWidget(),
                Divider(),
                HasGeoWidget(),
                Divider(),
                GeoContextWidget(),
                Divider(),
                //TODO: Map
                IsCommonsWidget(),
                Divider(),
                InGalleryWidget(),
                Divider(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
