import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/ui/ui_component/checkbox_three_value.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HasGeoWidget extends StatefulWidget {
  const HasGeoWidget({super.key});

  @override
  State<HasGeoWidget> createState() => _HasGeoWidgetState();
}

class _HasGeoWidgetState extends State<HasGeoWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text("Has Geo: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
        CheckboxThreeValue(
          value: _searchBloc.filter.hasGeo,
          onChanged: (value) => setState(() => _searchBloc.filter.hasGeo = value),
        )
      ],
    );
  }
}
