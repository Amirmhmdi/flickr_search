import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/media.dart';
import 'package:flickr_search/ui/ui_component/combo_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MediaWidget extends StatefulWidget {
  const MediaWidget({super.key});

  @override
  State<MediaWidget> createState() => _MediaWidgetState();
}

class _MediaWidgetState extends State<MediaWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      buildWhen: (previous, current) => current is UpdateMediaWidget,
      builder: (context, state) {
        return SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Media: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
              ComboBoxWidget(
                itemsList: Media.values.map((media) => media.value).toList(),
                onSelected: (value) {
                  _searchBloc.add(NewMediaValueEvent(mediaValue: value));
                },
                lable: "Media",
                selectedValue: _searchBloc.filter.media.value,
              ),
            ],
          ),
        );
      },
    );
  }
}
