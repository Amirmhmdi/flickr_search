import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class IsCommonsWidget extends StatefulWidget {
  const IsCommonsWidget({super.key});

  @override
  State<IsCommonsWidget> createState() => _IsCommonsWidgetState();
}

class _IsCommonsWidgetState extends State<IsCommonsWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text("Is Commons: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
        Checkbox(
          value: _searchBloc.filter.isCommons,
          onChanged: (value) {
            setState(() {
              _searchBloc.filter.isCommons = (value ?? false);
            });
          },
        )
      ],
    );
  }
}
