import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/get_time_widget/time_row.dart';
import 'package:flickr_search/ui/ui_component/date_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GetTimeWidget extends StatefulWidget {
  const GetTimeWidget({super.key});

  @override
  State<GetTimeWidget> createState() => _GetTimeWidgetState();
}

class _GetTimeWidgetState extends State<GetTimeWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      buildWhen: (previous, current) => current is UpdateDateWidget,
      builder: (context, state) {
        return Column(
          children: [
            TimeRow(
              text: "Min Uplaod Date :",
              timeStamp: _searchBloc.filter.minUploadDate,
              onGetDate: () async {
                int timeStamp = await datePickerBox(context);
                _searchBloc.add(NewMinUploadDataEvent(minUpload: timeStamp));
              },
              onDeleteDate: () {
                _searchBloc.add(NewMinUploadDataEvent(minUpload: -1));
              },
            ),
            TimeRow(
              text: "Max Uplaod Date :",
              timeStamp: _searchBloc.filter.maxUploadDate,
              onGetDate: () async {
                int timeStamp = await datePickerBox(context);
                _searchBloc.add(NewMaxUploadDataEvent(maxUpload: timeStamp));
              },
              onDeleteDate: () {
                _searchBloc.add(NewMaxUploadDataEvent(maxUpload: -1));
              },
            ),
            TimeRow(
              text: "Min Taken Date :",
              timeStamp: _searchBloc.filter.minTakenDate,
              onGetDate: () async {
                int timeStamp = await datePickerBox(context);
                _searchBloc.add(NewMinTakenDataEvent(minTaken: timeStamp));
              },
              onDeleteDate: () {
                _searchBloc.add(NewMinTakenDataEvent(minTaken: -1));
              },
            ),
            TimeRow(
              text: "Max Taken Date :",
              timeStamp: _searchBloc.filter.maxTakenDate,
              onGetDate: () async {
                int timeStamp = await datePickerBox(context);
                _searchBloc.add(NewMaxTakenDataEvent(maxTaken: timeStamp));
              },
              onDeleteDate: () {
                _searchBloc.add(NewMaxTakenDataEvent(maxTaken: -1));
              },
            ),
          ],
        );
      },
    );
  }
}
