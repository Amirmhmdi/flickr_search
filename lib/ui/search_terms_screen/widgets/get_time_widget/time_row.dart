import 'package:flutter/material.dart';

class TimeRow extends StatelessWidget {
  final String text;
  final int timeStamp;
  final Function onGetDate;
  final Function onDeleteDate;

  const TimeRow({
    super.key,
    required this.text,
    required this.timeStamp,
    required this.onGetDate,
    required this.onDeleteDate,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(text, style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
          (timeStamp == -1)
              ? TextButton(
                  onPressed: () {
                    onGetDate();
                  },
                  child: const Text(
                    "Choose date",
                    style: TextStyle(color: Colors.blue),
                  ),
                )
              : SizedBox(
                  height: 30.0,
                  child: Stack(
                    children: [
                      OutlinedButton(
                        onPressed: () {
                          onDeleteDate();
                        },
                        style: OutlinedButton.styleFrom(
                          padding: const EdgeInsets.all(4.0),
                          side: const BorderSide(width: 1.0, color: Colors.purple),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              DateTime.fromMillisecondsSinceEpoch(timeStamp).toString(),
                              style: const TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      const Positioned.fill(
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.cancel_rounded,
                            size: 14,
                            color: Colors.red,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
        ],
      ),
    );
  }
}
