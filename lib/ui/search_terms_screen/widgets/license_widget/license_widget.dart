import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/content_types.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LicenseWidget extends StatefulWidget {
  const LicenseWidget({super.key});

  @override
  State<LicenseWidget> createState() => _LicenseWidgetState();
}

class _LicenseWidgetState extends State<LicenseWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: BlocBuilder<SearchBloc, SearchState>(
        buildWhen: (previous, current) => (current is UpdateLicanceWidget || current is LicenseErrorState),
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                child: const Text(
                  "License: ",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 30,
                child: _searchBloc.licenseObj != null
                    ? ListView.separated(
                        padding: const EdgeInsets.only(top: 8.0),
                        scrollDirection: Axis.horizontal,
                        itemCount: _searchBloc.licenseObj?.licenses?.license?.length ?? 0,
                        separatorBuilder: (context, index) => const SizedBox(width: 8),
                        itemBuilder: (context, index) {
                          return OutlinedButton(
                            onPressed: () {
                              _searchBloc.add(NewLicenseToggleEvent(licenseID: _searchBloc.licenseObj?.licenses?.license?[index].id ?? -1));
                            },
                            style: OutlinedButton.styleFrom(
                              padding: const EdgeInsets.all(0.0),
                              side: const BorderSide(width: 1.0, color: Colors.purple),
                              backgroundColor:
                                  _searchBloc.filter.license.contains(_searchBloc.licenseObj?.licenses?.license?[index].id ?? -1) ? Colors.purpleAccent.shade100.withOpacity(0.5) : Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Text(
                                _searchBloc.licenseObj?.licenses?.license?[index].name ?? "",
                                style: const TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          );
                        },
                      )
                    : state is LicenseErrorState
                        ? TextButton(
                            onPressed: () {
                              _searchBloc.add(FetchLicanceEvent());
                            },
                            child: const Text(
                              "Try again",
                              style: TextStyle(
                                color: Colors.blue,
                              ),
                            ),
                          )
                        : const CircularProgressIndicator(),
              ),
            ],
          );
        },
      ),
    );
  }
}
