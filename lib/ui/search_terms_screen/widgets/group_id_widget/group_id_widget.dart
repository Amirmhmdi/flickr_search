import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GroupIdWidget extends StatefulWidget {
  const GroupIdWidget({super.key});

  @override
  State<GroupIdWidget> createState() => _GroupIdWidgetState();
}

class _GroupIdWidgetState extends State<GroupIdWidget> {
  late TextEditingController _textEditingController;
  late SearchBloc _searchBloc;
  OutlineInputBorder border = const OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
    borderSide: BorderSide(color: Colors.purple),
  );

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    _textEditingController = TextEditingController(text: _searchBloc.filter.groupId);
    super.initState();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text("Group ID: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
        const Spacer(),
        Expanded(
          child: TextField(
            controller: _textEditingController,
            scrollPadding: const EdgeInsets.all(0),
            cursorColor: Colors.black,
            textInputAction: TextInputAction.done,
            style: const TextStyle(color: Colors.black, fontSize: 15.0),
            decoration: InputDecoration(
              hintText: "Enter Group ID ...",
              contentPadding: const EdgeInsets.only(top: 0.0, bottom: 0.0, left: 16.0, right: 0.0),
              border: border,
              disabledBorder: border,
              enabledBorder: border,
              errorBorder: border,
              focusedBorder: border,
            ),
            onChanged: (value) {
              _searchBloc.filter.groupId = value;
            },
          ),
        ),
      ],
    );
  }
}
