import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/content_types.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContentTypeWidget extends StatefulWidget {
  const ContentTypeWidget({super.key});

  @override
  State<ContentTypeWidget> createState() => _ContentTypeWidgetState();
}

class _ContentTypeWidgetState extends State<ContentTypeWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: BlocBuilder<SearchBloc, SearchState>(
        buildWhen: (previous, current) => current is UpdateContentTypeWidget,
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text("Contents Type: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
              SizedBox(
                height: 50,
                child: ListView.separated(
                  padding: const EdgeInsets.only(top: 8.0),
                  scrollDirection: Axis.horizontal,
                  itemCount: ContentTypes.values.toList().length,
                  separatorBuilder: (context, index) => const SizedBox(width: 8),
                  itemBuilder: (context, index) {
                    return SingleChildScrollView(
                      child: SizedBox(
                        height: 30.0,
                        child: Stack(
                          children: [
                            OutlinedButton(
                              onPressed: () {
                                _searchBloc.add(NewContentTypeToggleEvent(contentTypes: ContentTypes.values[index]));
                              },
                              style: OutlinedButton.styleFrom(
                                padding: const EdgeInsets.all(4.0),
                                side: const BorderSide(width: 1.0, color: Colors.purple),
                                backgroundColor: _searchBloc.filter.contentTypes.contains(ContentTypes.values[index]) ? Colors.purpleAccent.shade100.withOpacity(0.5) : Colors.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  ContentTypes.values[index].value,
                                  style: const TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
