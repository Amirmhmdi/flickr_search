import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/ui/ui_component/checkbox_three_value.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InGalleryWidget extends StatefulWidget {
  const InGalleryWidget({super.key});

  @override
  State<InGalleryWidget> createState() => _InGalleryWidgetState();
}

class _InGalleryWidgetState extends State<InGalleryWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text("In Gallery: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
        Checkbox(
          value: _searchBloc.filter.inGallery,
          onChanged: (value) => setState(() => _searchBloc.filter.inGallery = (value ?? false)),
        )
      ],
    );
  }
}
