import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void showAddTagDialog(BuildContext context) {
  final TextEditingController textEditingController = TextEditingController();
  showDialog(
    context: context,
    builder: (ctx) {
      return AlertDialog(
        title: Container(
          child: Column(
            children: [
              const Text(
                "Write your tag and then press Add button",
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              TextField(
                controller: textEditingController,
                autofocus: true,
              ),
              const SizedBox(
                height: 8,
              ),
              OutlinedButton(
                onPressed: () {
                  BlocProvider.of<SearchBloc>(context).add(AddTagEvent(tagName: textEditingController.text));
                  Navigator.pop(context);
                },
                child: const Text("Add"),
              ),
            ],
          ),
        ),
      );
    },
  );
}
