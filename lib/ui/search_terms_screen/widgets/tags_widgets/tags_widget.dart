import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/tags_mode.dart';
import 'package:flickr_search/ui/search_terms_screen/widgets/tags_widgets/tags_add_dialog.dart';
import 'package:flickr_search/ui/ui_component/combo_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TagsWidget extends StatefulWidget {
  const TagsWidget({super.key});

  @override
  State<TagsWidget> createState() => _TagsWidgetState();
}

class _TagsWidgetState extends State<TagsWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      buildWhen: (previous, current) => (current is UpdateTagWidget),
      builder: (context, state) {
        return Column(
          children: [
            SizedBox(
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Tags:"),
                  ComboBoxWidget(
                    itemsList: TagsMode.values.map((tag) => tag.value).toList(),
                    selectedValue: _searchBloc.filter.tagsMode.value,
                    lable: "Mode",
                    onSelected: (value) {
                      _searchBloc.filter.tagsMode = stringToTagMode(value);
                    },
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.center,
              height: 30.0,
              padding: const EdgeInsets.only(left: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  OutlinedButton(
                    onPressed: () {
                      showAddTagDialog(context);
                    },
                    style: OutlinedButton.styleFrom(
                      padding: const EdgeInsets.all(4.0),
                      side: const BorderSide(width: 1.0, color: Colors.purple),
                    ),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Add Tag",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Icon(Icons.tag),
                      ],
                    ),
                  ),
                  const VerticalDivider(color: Colors.purple),
                  Expanded(
                    child: ListView.separated(
                      scrollDirection: Axis.horizontal,
                      itemCount: _searchBloc.filter.tagList.length,
                      separatorBuilder: (context, index) => const SizedBox(width: 8),
                      itemBuilder: (context, index) {
                        return SingleChildScrollView(
                          child: SizedBox(
                            height: 30.0,
                            child: Stack(
                              children: [
                                OutlinedButton(
                                  onPressed: () {
                                    _searchBloc.add(RemoveTagEvent(index: index));
                                  },
                                  style: OutlinedButton.styleFrom(
                                    padding: const EdgeInsets.all(4.0),
                                    side: const BorderSide(width: 1.0, color: Colors.purple),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        _searchBloc.filter.tagList[index],
                                        style: const TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                const Positioned.fill(
                                  child: Align(
                                    alignment: Alignment.topRight,
                                    child: Icon(
                                      Icons.cancel_rounded,
                                      size: 14,
                                      color: Colors.red,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
