import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/sort.dart';
import 'package:flickr_search/ui/ui_component/combo_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SortWidget extends StatefulWidget {
  const SortWidget({super.key});

  @override
  State<SortWidget> createState() => _SortWidgetState();
}

class _SortWidgetState extends State<SortWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      buildWhen: (previous, current) => current is UpdateSortWidget,
      builder: (context, state) {
        return SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Sort: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
              ComboBoxWidget(
                itemsList: Sort.values.map((sort) => sort.value).toList(),
                onSelected: (value) {
                  _searchBloc.add(NewSortValueEvent(sortValue: value));
                },
                lable: "Sort :",
                selectedValue: _searchBloc.filter.sort.value,
              ),
            ],
          ),
        );
      },
    );
  }
}
