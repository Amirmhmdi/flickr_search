import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/safe_search.dart';
import 'package:flickr_search/ui/ui_component/combo_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SafeSearchWidget extends StatefulWidget {
  const SafeSearchWidget({super.key});

  @override
  State<SafeSearchWidget> createState() => _SafeSearchWidgetState();
}

class _SafeSearchWidgetState extends State<SafeSearchWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      buildWhen: (previous, current) => current is UpdateSafeSearchWidget,
      builder: (context, state) {
        return SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Safe Search: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
              ComboBoxWidget(
                itemsList: SafaSearch.values.map((safe) => safe.value).toList(),
                onSelected: (value) {
                  _searchBloc.add(NewSafeSearchValueEvent(safeSearch: value));
                },
                lable: "Safe Search",
                selectedValue: _searchBloc.filter.safaSearch.value,
              ),
            ],
          ),
        );
      },
    );
  }
}
