import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/privacy_filter.dart';
import 'package:flickr_search/ui/ui_component/combo_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PrivacyFilterWidget extends StatefulWidget {
  const PrivacyFilterWidget({super.key});

  @override
  State<PrivacyFilterWidget> createState() => _PrivacyFilterWidgetState();
}

class _PrivacyFilterWidgetState extends State<PrivacyFilterWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      buildWhen: (previous, current) => current is UpdatePrivacyFilterWidget,
      builder: (context, state) {
        return SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Privacy Filter: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
              ComboBoxWidget(
                itemsList: PrivacyFilter.values.map((privacy) => privacy.value).toList(),
                onSelected: (value) {
                  _searchBloc.add(NewPivacyFilterValueEvent(privacyValue: value));
                },
                lable: "Privacy Filter",
                selectedValue: _searchBloc.filter.privacyFilter.value,
              ),
            ],
          ),
        );
      },
    );
  }
}
