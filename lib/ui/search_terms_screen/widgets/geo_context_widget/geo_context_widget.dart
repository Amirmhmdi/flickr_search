import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/geo_context.dart';
import 'package:flickr_search/ui/ui_component/combo_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GeoContextWidget extends StatefulWidget {
  const GeoContextWidget({super.key});

  @override
  State<GeoContextWidget> createState() => _GeoContextWidgetState();
}

class _GeoContextWidgetState extends State<GeoContextWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      buildWhen: (previous, current) => current is UpdateGeoContextWidget,
      builder: (context, state) {
        return SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Geo Context: ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
              ComboBoxWidget(
                itemsList: GeoContext.values.map((geo) => geo.value).toList(),
                onSelected: (value) {
                  _searchBloc.add(NewGeoContextValueEvent(geoContextValue: value));
                },
                lable: "Geo Context",
                selectedValue: _searchBloc.filter.geoContext.value,
              ),
            ],
          ),
        );
      },
    );
  }
}
