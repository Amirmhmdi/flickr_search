import 'package:flickr_search/blocs/search/search_bloc.dart';
import 'package:flickr_search/entities/enums/machine_tag_mode.dart';
import 'package:flickr_search/ui/ui_component/combo_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MachineTagModeWidget extends StatefulWidget {
  const MachineTagModeWidget({super.key});

  @override
  State<MachineTagModeWidget> createState() => _MachineTagModeWidgetState();
}

class _MachineTagModeWidgetState extends State<MachineTagModeWidget> {
  late SearchBloc _searchBloc;

  @override
  void initState() {
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text("Machine Tags Mode:"),
          ComboBoxWidget(
            itemsList: MachineTagMode.values.map((tag) => tag.value).toList(),
            selectedValue: _searchBloc.filter.machineTagMode.value,
            lable: "Mode",
            onSelected: (value) {
              _searchBloc.filter.machineTagMode = stringToMachineTagMode(value);
            },
          ),
        ],
      ),
    );
  }
}
