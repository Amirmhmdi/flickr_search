String getResposnseMessage(int statusCode) {
  String message = '';
  switch (statusCode) {
    case 1:
      message = "Too many tags in ALL query";
      break;
    case 2:
      message = "Unknown user";
      break;
    case 3:
      message = "Parameterless searches have been disabled";
      break;
    case 4:
      message = "You don't have permission to view this pool";
      break;
    case 5:
      message = "User deleted";
      break;
    case 10:
      message = "Sorry, the Flickr search API is not currently available.";
      break;
    case 11:
      message = "No valid machine tags";
      break;
    case 12:
      message = "Exceeded maximum allowable machine tags";
      break;
    case 17:
      message = "You can only search within your own contacts";
      break;
    case 18:
      message = "Illogical arguments";
      break;
    case 100:
      message = "Invalid API Key";
      break;
    case 105:
      message = "Service currently unavailable";
      break;
    case 106:
      message = "Write operation failed";
      break;
    case 111:
      message = "Format xxx not found";
      break;
    case 112:
      message = "Method xxx not found";
      break;
    case 114:
      message = "Invalid SOAP envelope";
      break;
    case 115:
      message = "Invalid XML-RPC Method Call";
      break;
    case 116:
      message = "Bad URL found";
      break;
    default:
      message = "Some thing were Wrone, Please try again";
  }
  return message;
}
