part of 'search_bloc.dart';

@immutable
sealed class SearchState {}

final class SearchInitialState extends SearchState {}

final class SearchLoadingState extends SearchState {}

final class SearchLoadedState extends SearchState {}

final class SearchErrorState extends SearchState {
  final String message;
  SearchErrorState({required this.message});
}

final class UpdateTagWidget extends SearchState {}

final class UpdateDateWidget extends SearchState {}

final class UpdateSortWidget extends SearchState {}

final class UpdatePrivacyFilterWidget extends SearchState {}

final class UpdateSafeSearchWidget extends SearchState {}

final class UpdateContentTypeWidget extends SearchState {}

final class UpdateVideoContentTypeWidget extends SearchState {}

final class UpdateContactWidget extends SearchState {}

final class UpdateMediaWidget extends SearchState {}

final class UpdateGeoContextWidget extends SearchState {}

final class UpdateLicanceWidget extends SearchState {}

final class LicenseErrorState extends SearchState {
  final String message;
  LicenseErrorState({required this.message});
}
