import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flickr_search/blocs/function/check_status_code.dart';
import 'package:flickr_search/entities/api_urls.dart';
import 'package:flickr_search/entities/constants.dart';
import 'package:flickr_search/entities/data_transfer_object/failure.dart';
import 'package:flickr_search/entities/data_transfer_object/licence/licence.dart';
import 'package:flickr_search/entities/data_transfer_object/photos_search.dart';
import 'package:flickr_search/entities/enums/contacts.dart';
import 'package:flickr_search/entities/enums/content_types.dart';
import 'package:flickr_search/entities/enums/geo_context.dart';
import 'package:flickr_search/entities/enums/media.dart';
import 'package:flickr_search/entities/enums/privacy_filter.dart';
import 'package:flickr_search/entities/enums/safe_search.dart';
import 'package:flickr_search/entities/enums/sort.dart';
import 'package:flickr_search/entities/enums/video_content_types.dart';
import 'package:flickr_search/entities/filters/filter.dart';
import 'package:flickr_search/repositories/search_repository/seach_repository.dart';
import 'package:meta/meta.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchRepository searchRepository;
  PhotosSearch? photosSearch;
  Filter filter = Filter();
  LicenseClass? licenseObj;
  SearchBloc({required this.searchRepository}) : super(SearchInitialState()) {
    on<NewSearchEvent>((event, emit) async {
      emit(SearchLoadingState());
      if (event.searchText != null) {
        filter.text = event.searchText ?? filter.text;
      }
      Uri uri = getIt<ApiUrl>().filckerPhotoSearch(filter.toQueryParameter());
      Either<Failure, PhotosSearch> result = await searchRepository.getPhotosData(uri);
      result.fold(
        (faliure) {
          String message = "";
          if (faliure is ServerFailure) {
            message = "Some thing were Wrone, Please try again";
          } else if (faliure is ResultFailure) {
            message = getResposnseMessage(faliure.statusCode);
          }
          emit(SearchErrorState(message: message));
        },
        (photosSearchInput) {
          photosSearch = photosSearchInput;
          emit(SearchLoadedState());
        },
      );
    });

    //Tag widget update
    on<RemoveTagEvent>((event, emit) async {
      filter.tagList.removeAt(event.index);
      emit(UpdateTagWidget());
    });
    on<AddTagEvent>((event, emit) async {
      filter.tagList.add(event.tagName);
      emit(UpdateTagWidget());
    });

    //Date widget update
    on<NewMinUploadDataEvent>((event, emit) async {
      filter.minUploadDate = event.minUpload;
      emit(UpdateDateWidget());
    });
    on<NewMaxUploadDataEvent>((event, emit) async {
      filter.maxUploadDate = event.maxUpload;
      emit(UpdateDateWidget());
    });
    on<NewMinTakenDataEvent>((event, emit) async {
      filter.minTakenDate = event.minTaken;
      emit(UpdateDateWidget());
    });
    on<NewMaxTakenDataEvent>((event, emit) async {
      filter.maxTakenDate = event.maxTaken;
      emit(UpdateDateWidget());
    });

    //Sort Widget Update
    on<NewSortValueEvent>((event, emit) async {
      filter.sort = stringToSort(event.sortValue);
      emit(UpdateSortWidget());
    });

    //Privacy Filter Update
    on<NewPivacyFilterValueEvent>((event, emit) async {
      filter.privacyFilter = stringToPrivacyFilter(event.privacyValue);
      emit(UpdatePrivacyFilterWidget());
    });

    //Safe search Update
    on<NewSafeSearchValueEvent>((event, emit) async {
      filter.safaSearch = stringToSafeSearch(event.safeSearch);
      emit(UpdateSafeSearchWidget());
    });

    //Content Type Update
    on<NewContentTypeToggleEvent>((event, emit) async {
      if (filter.contentTypes.contains(event.contentTypes)) {
        filter.contentTypes.remove(event.contentTypes);
      } else {
        filter.contentTypes.add(event.contentTypes);
      }
      emit(UpdateContentTypeWidget());
    });

    //Video Content Type Update
    on<NewVideoContentTypeToggleEvent>((event, emit) async {
      if (filter.videoContentTypes.contains(event.videoContentTypes)) {
        filter.videoContentTypes.remove(event.videoContentTypes);
      } else {
        filter.videoContentTypes.add(event.videoContentTypes);
      }
      emit(UpdateVideoContentTypeWidget());
    });

    //Contact Update
    on<NewContactValueEvent>((event, emit) async {
      filter.contacts = stringToContact(event.contactValue);
      emit(UpdateContactWidget());
    });

    //media Update
    on<NewMediaValueEvent>((event, emit) async {
      filter.media = stringToMedia(event.mediaValue);
      emit(UpdateMediaWidget());
    });

    //Geo Context Update
    on<NewGeoContextValueEvent>((event, emit) async {
      filter.geoContext = stringToGeoContext(event.geoContextValue);
      emit(UpdateGeoContextWidget());
    });

    //License api get
    on<FetchLicanceEvent>((event, emit) async {
      emit(UpdateLicanceWidget());
      if (licenseObj == null) {
        Uri uri = getIt<ApiUrl>().filckerLicense();
        Either<Failure, LicenseClass> result = await searchRepository.getLicenceData(uri);
        result.fold(
          (faliure) {
            String message = "";
            if (faliure is ServerFailure) {
              message = "Some thing were Wrone, Please try again";
            } else if (faliure is ResultFailure) {
              message = getResposnseMessage(faliure.statusCode);
            }
            emit(LicenseErrorState(message: message));
          },
          (licenseObjInput) {
            licenseObj = licenseObjInput;
            emit(UpdateLicanceWidget());
          },
        );
      }
    });

    //License Update
    on<NewLicenseToggleEvent>((event, emit) async {
      if (filter.license.contains(event.licenseID)) {
        filter.license.remove(event.licenseID);
      } else {
        filter.license.add(event.licenseID);
      }
      emit(UpdateLicanceWidget());
    });
  }
}
