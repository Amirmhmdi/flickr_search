part of 'search_bloc.dart';

@immutable
sealed class SearchEvent {}

final class NewSearchEvent extends SearchEvent {
  final String? searchText;

  NewSearchEvent({this.searchText});
}

final class RemoveTagEvent extends SearchEvent {
  final int index;

  RemoveTagEvent({required this.index});
}

final class AddTagEvent extends SearchEvent {
  final String tagName;

  AddTagEvent({required this.tagName});
}

final class NewMinUploadDataEvent extends SearchEvent {
  final int minUpload;

  NewMinUploadDataEvent({required this.minUpload});
}

final class NewMaxUploadDataEvent extends SearchEvent {
  final int maxUpload;

  NewMaxUploadDataEvent({required this.maxUpload});
}

final class NewMinTakenDataEvent extends SearchEvent {
  final int minTaken;

  NewMinTakenDataEvent({required this.minTaken});
}

final class NewMaxTakenDataEvent extends SearchEvent {
  final int maxTaken;

  NewMaxTakenDataEvent({required this.maxTaken});
}

final class NewSortValueEvent extends SearchEvent {
  final String sortValue;

  NewSortValueEvent({required this.sortValue});
}

final class NewPivacyFilterValueEvent extends SearchEvent {
  final String privacyValue;

  NewPivacyFilterValueEvent({required this.privacyValue});
}

final class NewSafeSearchValueEvent extends SearchEvent {
  final String safeSearch;

  NewSafeSearchValueEvent({required this.safeSearch});
}

final class NewContentTypeToggleEvent extends SearchEvent {
  final ContentTypes contentTypes;

  NewContentTypeToggleEvent({required this.contentTypes});
}

final class NewVideoContentTypeToggleEvent extends SearchEvent {
  final VideoContentTypes videoContentTypes;

  NewVideoContentTypeToggleEvent({required this.videoContentTypes});
}

final class NewContactValueEvent extends SearchEvent {
  final String contactValue;

  NewContactValueEvent({required this.contactValue});
}

final class NewMediaValueEvent extends SearchEvent {
  final String mediaValue;

  NewMediaValueEvent({required this.mediaValue});
}

final class NewGeoContextValueEvent extends SearchEvent {
  final String geoContextValue;

  NewGeoContextValueEvent({required this.geoContextValue});
}

final class FetchLicanceEvent extends SearchEvent {}

final class NewLicenseToggleEvent extends SearchEvent {
  final int licenseID;

  NewLicenseToggleEvent({required this.licenseID});
}
