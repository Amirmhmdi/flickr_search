import 'package:flickr_search/entities/constants.dart';

class ApiUrl {
  final String _baseUrl = "https://www.flickr.com/services/rest/";

  Uri filckerPhotoSearch(String qureyParamers) {
    return Uri.parse("$_baseUrl?method=flickr.photos.search$qureyParamers");
  }

  Uri filckerLicense() {
    return Uri.parse("$_baseUrl?method=flickr.photos.licenses.getInfo&api_key=$api_key&format=json&nojsoncallback=1");
  }
}
