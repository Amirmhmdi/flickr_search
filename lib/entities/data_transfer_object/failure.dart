abstract class Failure {}

class ServerFailure extends Failure {}

class ResultFailure extends Failure {
  final int statusCode;
  ResultFailure({required this.statusCode});
}
