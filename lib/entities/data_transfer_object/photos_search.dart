import 'package:flickr_search/entities/data_transfer_object/photos.dart';

class PhotosSearch {
  Photos? photos;
  String? stat;

  PhotosSearch({this.photos, this.stat});

  PhotosSearch.fromJson(Map<String, dynamic> json) {
    photos = json['photos'] != null ? Photos.fromJson(json['photos']) : null;
    stat = json['stat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (this.photos != null) {
      data['photos'] = this.photos!.toJson();
    }
    data['stat'] = this.stat;
    return data;
  }
}


