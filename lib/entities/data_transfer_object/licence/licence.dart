class LicenseClass {
  Licenses? licenses;
  String? stat;

  LicenseClass({this.licenses, this.stat});

  LicenseClass.fromJson(Map<String, dynamic> json) {
    licenses = json['licenses'] != null
        ? new Licenses.fromJson(json['licenses'])
        : null;
    stat = json['stat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.licenses != null) {
      data['licenses'] = this.licenses!.toJson();
    }
    data['stat'] = this.stat;
    return data;
  }
}

class Licenses {
  List<License>? license;

  Licenses({this.license});

  Licenses.fromJson(Map<String, dynamic> json) {
    if (json['license'] != null) {
      license = <License>[];
      json['license'].forEach((v) {
        license!.add(new License.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.license != null) {
      data['license'] = this.license!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class License {
  int? id;
  String? name;
  String? url;

  License({this.id, this.name, this.url});

  License.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['url'] = this.url;
    return data;
  }
}