enum GeoContext {
  notDefined,
  indoors,
  outdoors,
  unselected,
}

extension TagsModeValue on GeoContext {
  String get value {
    switch (this) {
      case GeoContext.notDefined:
        return 'notDefined';
      case GeoContext.indoors:
        return 'indoors';
      case GeoContext.outdoors:
        return 'outdoors';
      case GeoContext.unselected:
        return 'unselected';
    }
  }

  String get valueParams {
    switch (this) {
      case GeoContext.notDefined:
        return '0';
      case GeoContext.indoors:
        return '1';
      case GeoContext.outdoors:
        return '2';
      case GeoContext.unselected:
        return '';
    }
  }
}

GeoContext stringToGeoContext(String value) {
  switch (value) {
    case 'notDefined':
      return GeoContext.notDefined;
    case 'indoors':
      return GeoContext.indoors;
    case 'outdoors':
      return GeoContext.outdoors;
    default:
      return GeoContext.unselected;
  }
}
