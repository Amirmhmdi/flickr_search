enum Media {
  all,
  photos,
  videos,
  undefined,
}

extension TagsModeValue on Media {
  String get value {
    switch (this) {
      case Media.all:
        return 'all';
      case Media.photos:
        return 'photos';
      case Media.videos:
        return 'videos';
      case Media.undefined:
        return 'undefined';
    }
  }

  String get valueParams {
    switch (this) {
      case Media.all:
        return 'all';
      case Media.photos:
        return 'photos';
      case Media.videos:
        return 'videos';
      case Media.undefined:
        return 'undefined';
    }
  }
}

Media stringToMedia(String value) {
  switch (value) {
    case 'all':
      return Media.all;
    case 'photos':
      return Media.photos;
    case 'videos':
      return Media.videos;
    default:
      return Media.undefined;
  }
}
