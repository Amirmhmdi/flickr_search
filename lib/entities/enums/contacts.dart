enum Contacts {
  all,
  ff,
  undefined,
}

extension TagsModeValue on Contacts {
  String get value {
    switch (this) {
      case Contacts.all:
        return 'all';
      case Contacts.ff:
        return 'friend & family';
      case Contacts.undefined:
        return 'undefined';
    }
  }

  String get valueParams {
    switch (this) {
      case Contacts.all:
        return 'all';
      case Contacts.ff:
        return 'ff';
      case Contacts.undefined:
        return 'undefined';
    }
  }
}

Contacts stringToContact(String value) {
  switch (value) {
    case 'all':
      return Contacts.all;
    case 'friend & family':
      return Contacts.ff;
    default:
      return Contacts.undefined;
  }
}
