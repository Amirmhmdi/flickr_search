enum ContentTypes {
  photos,
  screenshots,
  other,
  virtualPhotos,
}

extension TagsModeValue on ContentTypes {
  String get value {
    switch (this) {
      case ContentTypes.photos:
        return 'photos';
      case ContentTypes.screenshots:
        return 'screenshots';
      case ContentTypes.other:
        return 'other';
      case ContentTypes.virtualPhotos:
        return 'virtualPhotos';
    }
  }

  String get valueParams {
    switch (this) {
      case ContentTypes.photos:
        return '0';
      case ContentTypes.screenshots:
        return '1';
      case ContentTypes.other:
        return '2';
      case ContentTypes.virtualPhotos:
        return '3';
    }
  }
}

ContentTypes stringToContentTypes(String value) {
  switch (value) {
    case 'photos':
      return ContentTypes.photos;
    case 'screenshots':
      return ContentTypes.screenshots;
    case 'other':
      return ContentTypes.other;
    case 'virtualPhotos':
      return ContentTypes.virtualPhotos;
    default:
      return ContentTypes.photos;
  }
}
