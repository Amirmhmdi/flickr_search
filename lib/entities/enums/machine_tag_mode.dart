enum MachineTagMode {
  all,
  any,
  undefined,
}

extension TagsModeValue on MachineTagMode {
  String get value {
    switch (this) {
      case MachineTagMode.all:
        return 'All';
      case MachineTagMode.any:
        return 'Any';
      case MachineTagMode.undefined:
        return 'undefined';
    }
  }

  String get valueParams {
    switch (this) {
      case MachineTagMode.all:
        return 'all';
      case MachineTagMode.any:
        return 'any';
      case MachineTagMode.undefined:
        return 'undefined';
    }
  }
}

MachineTagMode stringToMachineTagMode(String value) {
  switch (value) {
    case 'All':
      return MachineTagMode.all;
    case 'Any':
      return MachineTagMode.any;
    default:
      return MachineTagMode.undefined;
  }
}
