enum PrivacyFilter {
  public,
  privateToFriends,
  privateToFamily,
  privateToFriendsFamily,
  completelyPrivatePhotos,
  undefined,
}

extension TagsModeValue on PrivacyFilter {
  String get value {
    switch (this) {
      case PrivacyFilter.public:
        return 'public';
      case PrivacyFilter.privateToFriends:
        return 'privateToFriends';
      case PrivacyFilter.privateToFamily:
        return 'privateToFamily';
      case PrivacyFilter.privateToFriendsFamily:
        return 'privateToFriendsFamily';
      case PrivacyFilter.completelyPrivatePhotos:
        return 'completelyPrivatePhotos';
      case PrivacyFilter.undefined:
        return 'undefined';
    }
  }

  String get valueParams {
    switch (this) {
      case PrivacyFilter.public:
        return '1';
      case PrivacyFilter.privateToFriends:
        return '2';
      case PrivacyFilter.privateToFamily:
        return '3';
      case PrivacyFilter.privateToFriendsFamily:
        return '4';
      case PrivacyFilter.completelyPrivatePhotos:
        return '5';
      case PrivacyFilter.undefined:
        return '0';
    }
  }
}

PrivacyFilter stringToPrivacyFilter(String value) {
  switch (value) {
    case 'public':
      return PrivacyFilter.public;
    case 'privateToFriends':
      return PrivacyFilter.privateToFriends;
    case 'privateToFamily':
      return PrivacyFilter.privateToFamily;
    case 'privateToFriendsFamily':
      return PrivacyFilter.privateToFriendsFamily;
    case 'completelyPrivatePhotos':
      return PrivacyFilter.completelyPrivatePhotos;
    default:
      return PrivacyFilter.undefined;
  }
}
