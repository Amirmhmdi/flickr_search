enum VideoContentTypes {
  videos,
  screencasts,
  animationCGI,
  machinima,
}

extension TagsModeValue on VideoContentTypes {
  String get value {
    switch (this) {
      case VideoContentTypes.videos:
        return 'videos';
      case VideoContentTypes.screencasts:
        return 'screencasts';
      case VideoContentTypes.animationCGI:
        return 'animationCGI';
      case VideoContentTypes.machinima:
        return 'machinima';
    }
  }

  String get valueParams {
    switch (this) {
      case VideoContentTypes.videos:
        return '0';
      case VideoContentTypes.screencasts:
        return '1';
      case VideoContentTypes.animationCGI:
        return '2';
      case VideoContentTypes.machinima:
        return '3';
    }
  }
}

VideoContentTypes stringToContentTypes(String value) {
  switch (value) {
    case 'videos':
      return VideoContentTypes.videos;
    case 'screencasts':
      return VideoContentTypes.screencasts;
    case 'animationCGI':
      return VideoContentTypes.animationCGI;
    case 'machinima':
      return VideoContentTypes.machinima;
    default:
      return VideoContentTypes.videos;
  }
}
