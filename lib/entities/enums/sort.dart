enum Sort {
  datePostedAsc,
  datePostedDesc,
  dateTakenAsc,
  dateTakenDesc,
  interestingnessDesc,
  interestingnessAsc,
  relevance,
  undefined,
}

extension SortValueUi on Sort {
  String get value {
    switch (this) {
      case Sort.datePostedAsc:
        return 'date Posted Asc';
      case Sort.datePostedDesc:
        return 'date Posted Desc';
      case Sort.dateTakenAsc:
        return 'date Taken Asc';
      case Sort.dateTakenDesc:
        return 'date Taken Desc';
      case Sort.interestingnessDesc:
        return 'interestingness Desc';
      case Sort.interestingnessAsc:
        return 'interestingness Asc';
      case Sort.relevance:
        return 'relevance';
      case Sort.undefined:
        return 'undefined';
    }
  }

  String get valueParams {
    switch (this) {
      case Sort.datePostedAsc:
        return 'date-posted-asc';
      case Sort.datePostedDesc:
        return 'date-posted-desc';
      case Sort.dateTakenAsc:
        return 'date-taken-asc';
      case Sort.dateTakenDesc:
        return 'date-taken-desc';
      case Sort.interestingnessDesc:
        return 'interestingness-desc';
      case Sort.interestingnessAsc:
        return 'interestingness-asc';
      case Sort.relevance:
        return 'relevance';
      case Sort.undefined:
        return 'undefined';
    }
  }
}

Sort stringToSort(String value) {
  switch (value) {
    case 'date Posted Asc':
      return Sort.datePostedAsc;
    case 'date Posted Desc':
      return Sort.datePostedDesc;
    case 'date Taken Asc':
      return Sort.dateTakenAsc;
    case 'date Taken Desc':
      return Sort.dateTakenDesc;
    case 'interestingness Desc':
      return Sort.interestingnessDesc;
    case 'interestingness Asc':
      return Sort.interestingnessAsc;
    case 'relevance':
      return Sort.relevance;
    default:
      return Sort.undefined;
  }
}
