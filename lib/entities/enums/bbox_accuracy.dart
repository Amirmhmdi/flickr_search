enum BBoxAccuracy {
  world,
  country,
  region,
  city,
  street,
  undefined,
}
