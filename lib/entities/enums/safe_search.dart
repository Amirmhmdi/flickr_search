enum SafaSearch {
  safe,
  moderate,
  restricted,
  undefined,
}

extension TagsModeValue on SafaSearch {
  String get value {
    switch (this) {
      case SafaSearch.safe:
        return 'safe';
      case SafaSearch.moderate:
        return 'moderate';
      case SafaSearch.restricted:
        return 'restricted';
      case SafaSearch.undefined:
        return 'undefined';
    }
  }

  String get valueParams {
    switch (this) {
      case SafaSearch.safe:
        return '1';
      case SafaSearch.moderate:
        return '2';
      case SafaSearch.restricted:
        return '3';
      case SafaSearch.undefined:
        return '0';
    }
  }
}

SafaSearch stringToSafeSearch(String value) {
  switch (value) {
    case 'safe':
      return SafaSearch.safe;
    case 'moderate':
      return SafaSearch.moderate;
    case 'restricted':
      return SafaSearch.restricted;
    default:
      return SafaSearch.undefined;
  }
}
