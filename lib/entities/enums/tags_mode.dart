enum TagsMode {
  all,
  any,
  undefined,
}

extension TagsModeValue on TagsMode {
  String get value {
    switch (this) {
      case TagsMode.all:
        return 'All';
      case TagsMode.any:
        return 'Any';
      case TagsMode.undefined:
        return 'undefined';
    }
  }

  String get valueParams {
    switch (this) {
      case TagsMode.all:
        return 'all';
      case TagsMode.any:
        return 'any';
      case TagsMode.undefined:
        return 'undefined';
    }
  }
}

TagsMode stringToTagMode(String value) {
  switch (value) {
    case 'All':
      return TagsMode.all;
    case 'Any':
      return TagsMode.any;
    default:
      return TagsMode.undefined;
  }
}
