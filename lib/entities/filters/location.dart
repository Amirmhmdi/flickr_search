import 'package:flickr_search/entities/enums/radius_type.dart';

class Location {
  int lat = -1;
  int lng = -1;
  int radius = -1;
  RadiusType radiusType = RadiusType.undefined;
}
