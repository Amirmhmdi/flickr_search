import 'package:flickr_search/entities/constants.dart';
import 'package:flickr_search/entities/enums/contacts.dart';
import 'package:flickr_search/entities/enums/content_types.dart';
import 'package:flickr_search/entities/enums/geo_context.dart';
import 'package:flickr_search/entities/enums/machine_tag_mode.dart';
import 'package:flickr_search/entities/enums/media.dart';
import 'package:flickr_search/entities/enums/privacy_filter.dart';
import 'package:flickr_search/entities/enums/safe_search.dart';
import 'package:flickr_search/entities/enums/sort.dart';
import 'package:flickr_search/entities/enums/tags_mode.dart';
import 'package:flickr_search/entities/enums/video_content_types.dart';
import 'package:flickr_search/entities/filters/bbox.dart';
import 'package:flickr_search/entities/filters/location.dart';

class Filter {
  String text = "";
  List<String> tagList = [];
  TagsMode tagsMode = TagsMode.undefined;
  int minUploadDate = -1;
  int maxUploadDate = -1;
  int minTakenDate = -1;
  int maxTakenDate = -1;
  List<int> license = [];
  Sort sort = Sort.undefined;
  PrivacyFilter privacyFilter = PrivacyFilter.undefined;
  BBox bBox = BBox();
  SafaSearch safaSearch = SafaSearch.undefined;
  List<ContentTypes> contentTypes = [];
  List<VideoContentTypes> videoContentTypes = [];
  MachineTagMode machineTagMode = MachineTagMode.undefined;
  String groupId = "";
  Contacts contacts = Contacts.undefined;
  String woeId = "";
  String placeId = "";
  Media media = Media.undefined;
  bool? hasGeo;
  GeoContext geoContext = GeoContext.unselected;
  Location location = Location();
  bool isCommons = false;
  bool inGallery = false;
  bool isGetty = false;

  String toQueryParameter() {
    String queryParameter = "";

    queryParameter += "&api_key=$api_key";

    if (tagList.isNotEmpty) {
      queryParameter += "&tags=${tagList.first}";
      for (var i = 1; i < tagList.length; i++) {
        queryParameter += "%2C${tagList[i]}";
      }
    }

    if (tagsMode != TagsMode.undefined) queryParameter += "&tag_mode=${tagsMode.valueParams}";

    if (text != "") queryParameter += "&text=$text";

    if (minUploadDate != -1) queryParameter += "&min_upload_date=${minUploadDate.toString()}";

    if (maxUploadDate != -1) queryParameter += "&max_upload_date=${maxUploadDate.toString()}";

    if (minTakenDate != -1) queryParameter += "&min_taken_date=${minTakenDate.toString()}";

    if (maxTakenDate != -1) queryParameter += "&max_taken_date=${maxTakenDate.toString()}";

    if (license.isNotEmpty) {
      queryParameter += "&license=${license.first.toString()}";
      for (var i = 1; i < tagList.length; i++) {
        queryParameter += "%2C${license[i].toString()}";
      }
    }

    if (sort != Sort.undefined) queryParameter += "&sort=${sort.valueParams}";

    if (privacyFilter != PrivacyFilter.undefined) queryParameter += "&privacy_filter=${privacyFilter.valueParams}";

    if (safaSearch != SafaSearch.undefined) queryParameter += "&safe_search=${safaSearch.valueParams}";

    if (contentTypes.isNotEmpty) {
      queryParameter += "&content_types=${contentTypes.first.valueParams}";
      for (var i = 1; i < contentTypes.length; i++) {
        queryParameter += "%2C${contentTypes[i].valueParams}";
      }
    }

    if (videoContentTypes.isNotEmpty) {
      queryParameter += "&video_content_types=${videoContentTypes.first.valueParams}";
      for (var i = 1; i < videoContentTypes.length; i++) {
        queryParameter += "%2C${videoContentTypes[i].valueParams}";
      }
    }

    if (machineTagMode != MachineTagMode.undefined) queryParameter += "&machine_tag_mode=${machineTagMode.valueParams}";

    if (groupId != "") queryParameter += "&group_id=$groupId";

    if (contacts != Contacts.undefined) queryParameter += "&contacts=${contacts.valueParams}";

    if (woeId != "") queryParameter += "&woe_id=$woeId";

    if (placeId != "") queryParameter += "&place_id=$placeId";

    if (media != Media.undefined) queryParameter += "&media=${media.valueParams}";

    if (hasGeo != null) queryParameter += "&has_geo=${((hasGeo ?? true) ? 1 : 0)}";

    if (geoContext != GeoContext.unselected) queryParameter += "&geo_context=${geoContext.valueParams}";

    if (isCommons != false) queryParameter += "&is_commons=${isCommons.toString()}";

    if (inGallery != false) queryParameter += "&in_gallery=${inGallery.toString()}";

    if (isGetty != false) queryParameter += "&is_getty=${isGetty.toString()}";

    queryParameter += "&per_page=25";
    queryParameter += "&page=1";
    queryParameter += "&format=json";
    queryParameter += "&nojsoncallback=1";

    return queryParameter;
  }

  List<String> listOfActiveFilter() {
    List<String> activeList = [];
    if (tagList.isNotEmpty) activeList.add("tagList");

    if (tagsMode != TagsMode.undefined) activeList.add("tagsMode");

    if (minUploadDate != -1) activeList.add("minUploadDate");

    if (maxUploadDate != -1) activeList.add("maxUploadDate");

    if (minTakenDate != -1) activeList.add("minTakenDate");

    if (maxTakenDate != -1) activeList.add("maxTakenDate");

    if (license.isNotEmpty) activeList.add("license");

    if (sort != Sort.undefined) activeList.add("sort");

    if (privacyFilter != PrivacyFilter.undefined) activeList.add("privacyFilter");

    if (safaSearch != SafaSearch.undefined) activeList.add("safaSearch");

    if (contentTypes.isNotEmpty) activeList.add("contentTypes");

    if (videoContentTypes.isNotEmpty) activeList.add("videoContentTypes");

    if (machineTagMode != MachineTagMode.undefined) activeList.add("machineTagMode");

    if (groupId != "") activeList.add("groupId");

    if (contacts != Contacts.undefined) activeList.add("contacts");

    if (woeId != "") activeList.add("woeId");

    if (placeId != "") activeList.add("placeId");

    if (media != Media.undefined) activeList.add("media");

    if (hasGeo != null) activeList.add("hasGeo");

    if (geoContext != GeoContext.unselected) activeList.add("geoContext");

    if (isCommons != false) activeList.add("isCommons");

    if (inGallery != false) activeList.add("inGallery");

    if (isGetty != false) activeList.add("isGetty");
    return activeList;
  }
}
