import 'package:flickr_search/entities/enums/bbox_accuracy.dart';

class BBox {
  int minimumLongitude = -1;
  int minimumLatitude = -1;
  int maximumLongitude = -1;
  int maximumLatitude = -1;
  BBoxAccuracy accuracy = BBoxAccuracy.undefined;
}
